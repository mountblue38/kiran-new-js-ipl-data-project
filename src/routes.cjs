const Papa = require("papaparse");
const fs = require("fs");
const path = require("path");
// Data Paths

const matchesFilePath = path.join(__dirname, "./data/matches.csv");
const deliveryFilePath = path.join(__dirname, "./data/deliveries.csv");

// problem paths

const problem1Path = path.join(__dirname, "./server/1-matches-per-year.cjs");
const problem2Path = path.join(__dirname, "./server/2-matches-won-per-team-per-year.cjs");
const problem3Path = path.join(__dirname, "./server/3-extra-runs-conceded-per-team-in2016.cjs");
const problem4Path = path.join(__dirname, "./server/4-top10-economical-bowler-2015.cjs");
const problem5Path = path.join(__dirname, "./server/5-number-of-times-team-won-both-toss-match.cjs");
const problem6Path = path.join(__dirname, "./server/6-most-man-of-the-match-per-season.cjs");
const problem7Path = path.join(__dirname, "./server/7-strike-rate-of-batsman-per-season.cjs");
const problem8Path = path.join(__dirname, "./server/8-highest-number-of-times-one-player-has-been-dismissed.cjs");
const problem9Path = path.join(__dirname, "./server/9-bowler-with-the-best-economy-in-super-overs.cjs");

//output paths

const outputFilePathP1 = path.join(__dirname, "./public/output/1-matches-per-year.json");
const outputFilePathP2 = path.join(__dirname, "./public/output/2-matches-won-per-team-per-year.json");
const outputFilePathP3 = path.join(__dirname, "./public/output/3-extra-runs-conceded-per-team-in2016.json");
const outputFilePathP4 = path.join(__dirname, "./public/output/4-top10-economical-bowler-2015.json");
const outputFilePathP5 = path.join(__dirname, "./public/output/5-number-of-times-team-won-both-toss-match.json");
const outputFilePathP6 = path.join(__dirname, "./public/output/6-most-man-of-the-match-per-season.json");
const outputFilePathP7 = path.join(__dirname, "./public/output/7-strike-rate-of-batsman-per-season.json");
const outputFilePathP8 = path.join(__dirname, "./public/output/8-highest-number-of-times-one-player-has-been-dismissed.json");
const outputFilePathP9 = path.join(__dirname, "./public/output/9-bowler-with-the-best-economy-in-super-overs.json");

// HTML and CSS path

const htmlPath = path.join(__dirname,"./index.html");

// parsing

const matchesFile = fs.readFileSync(matchesFilePath);
const matchesData = Papa.parse(matchesFile.toString(), {
    header: true,
    skipEmptyLines: true,
});

const deliveriesfile = fs.readFileSync(deliveryFilePath);
const deliveriesData = Papa.parse(deliveriesfile.toString(), {
    header: true,
    skipEmptyLines: true,
});

module.exports = (app) => {
    // // home

    app.get('/',(req,res)=>{
        res.sendFile(htmlPath);
    })

    // // problem1

    app.get("/problem1", (req, res) => {
        const problem1 = require(problem1Path);
        try {
            fs.writeFileSync(outputFilePathP1, JSON.stringify(problem1(matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP1);
    });

    // // problem2

    app.get("/problem2", (req, res) => {
        const problem2 = require(problem2Path);

        try {
            fs.writeFileSync(outputFilePathP2, JSON.stringify(problem2(matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP2);
    });

    // // problem3

    app.get("/problem3", (req, res) => {
        const problem3 = require(problem3Path);

        try {
            fs.writeFileSync(outputFilePathP3, JSON.stringify(problem3(deliveriesData, matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP3);
    });

    // // problem4

    app.get("/problem4", (req, res) => {
        const problem4 = require(problem4Path);

        try {
            fs.writeFileSync(outputFilePathP4, JSON.stringify(problem4(deliveriesData, matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP4);
    });

    // // problem5

    app.get("/problem5", (req, res) => {
        const problem5 = require(problem5Path);

        try {
            fs.writeFileSync(outputFilePathP5, JSON.stringify(problem5(matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP5);
    });

    // // problem6

    app.get("/problem6", (req, res) => {
        const problem6 = require(problem6Path);

        try {
            fs.writeFileSync(outputFilePathP6, JSON.stringify(problem6(matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP6);
    });

    // // problem7

    app.get("/problem7", (req, res) => {
        const problem7 = require(problem7Path);

        try {
            fs.writeFileSync(outputFilePathP7, JSON.stringify(problem7(deliveriesData, matchesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP7);
    });

    // // problem8

    app.get("/problem8", (req, res) => {
        const problem8 = require(problem8Path);

        try {
            fs.writeFileSync(outputFilePathP8, JSON.stringify(problem8(deliveriesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP8);
    });

    // // problem9

    app.get("/problem9", (req, res) => {
        const problem9 = require(problem9Path);

        try {
            fs.writeFileSync(outputFilePathP9, JSON.stringify(problem9(deliveriesData), null, 4));
        } catch (error) {
            console.log("An error has occurred ", error);
        }
        res.sendFile(outputFilePathP9);
    });
};
