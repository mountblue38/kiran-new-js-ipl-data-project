const express = require("express");
const expressApp = require("./expressApp.cjs");

const app = express();
const port = process.env.PORT || 3000;

expressApp(app);

app.listen(port, () => {
    console.log(`Listening in the port ${port}`);
}).on("error", (error) => {
    console.log(error);
});
