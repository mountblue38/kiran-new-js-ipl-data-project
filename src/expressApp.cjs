const express = require("express");
const cors = require("cors");
const routes = require("./routes.cjs");

module.exports = (app) => {
  app.use(express.json({ limit: "1mb" }));
  app.use(express.urlencoded({ extended: true, limit: "1mb" }));
  app.use(cors());
  app.use(express.static(__dirname));

  // Routes
  routes(app);

};