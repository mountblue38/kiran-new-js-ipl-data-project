function problem3(deliverysData, matchesData) {
    let filteredData = matchesData.data.filter((element) => {
        return element.season == "2016";
    });

    let ans = filteredData.reduce((outputObj, matchData) => {
        deliverysData.data.map((delData) => {
            if (matchData.id == delData.match_id) {
                if (!(delData.bowling_team in outputObj)) {
                    outputObj[delData.bowling_team] = 0;
                }
                if (delData.extra_runs > 0) {
                    outputObj[delData.bowling_team] += Number(delData.extra_runs);
                }
            }
            return delData;
        });
        return outputObj;
    }, {});

    return ans;
}

module.exports = problem3;
