function problem5(returedArrayOfObjects) {
    let outputObj = returedArrayOfObjects.data.reduce((acc, objectData) => {
        if (!(objectData.season in acc)) {
            acc[objectData.season] = {};
        }
        if (objectData.winner == objectData.toss_winner) {
            if (objectData.winner in acc[objectData.season]) {
                acc[objectData.season][objectData.winner] += 1;
            } else {
                acc[objectData.season][objectData.winner] = 1;
            }
        }
        return acc;
    }, {});

    return outputObj;
}

module.exports = problem5;
