function problem9(returedArrayOfObjects) {
    let bestEcoCount = Infinity,
        bestEcoPlayer;

    let superBowler = returedArrayOfObjects.data.reduce((acc, delData) => {
        if (delData.is_super_over == 1) {
            if (!(delData.bowler in acc)) {
                acc[delData.bowler] = { runs: 0, balls: 0 };
            }
            acc[delData.bowler].runs += Number(delData.total_runs);
            acc[delData.bowler].balls += 1;
        }
        return acc;
    }, {});

    Object.keys(superBowler).map((year) => {
        let economy = Number(((superBowler[year].runs / superBowler[year].balls) * 6).toFixed(2));
        if (bestEcoCount > economy) {
            bestEcoCount = economy;
            bestEcoPlayer = year;
        }
        return superBowler;
    });

    return { "bestEcoPlayer": bestEcoPlayer, "bestEcoCount": bestEcoCount };
}

module.exports = problem9;
