function problem7(deliverysData, matchesData) {
    let res = matchesData.data.reduce((outputObj, matchData) => {
        deliverysData.data.map((delData) => {
            if (!(matchData.season in outputObj)) {
                outputObj[matchData.season] = {};
            }
            if (matchData.id == delData.match_id) {
                if (!(delData.batsman in outputObj[matchData.season])) {
                    outputObj[matchData.season][delData.batsman] = { runs: 0, balls: 0 };
                }
                outputObj[matchData.season][delData.batsman].runs += Number(delData.batsman_runs);
                outputObj[matchData.season][delData.batsman].balls += 1;
            }
            return delData;
        });
        return outputObj;
    }, {});

    Object.keys(res).map((year) => {
        Object.keys(res[year]).map((player) => {
            res[year][player] = ((res[year][player].runs / res[year][player].balls) * 100).toFixed(2);
            return player;
        });
        return res;
    });

    return res;
}

module.exports = problem7;
