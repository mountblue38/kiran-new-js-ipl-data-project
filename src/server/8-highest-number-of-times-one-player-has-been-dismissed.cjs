function problem8(returedArrayOfObjects) {
    let maxDisCount = 0;
    let batsman, bowler;

    returedArrayOfObjects.data.reduce((dismissedData, delData) => {
        if (!(delData.player_dismissed in dismissedData && delData.player_dismissed != "")) {
            dismissedData[delData.player_dismissed] = {};
        }

        if (!(delData.bowler in dismissedData[delData.player_dismissed])) {
            dismissedData[delData.player_dismissed][delData.bowler] = 0;
        }
        dismissedData[delData.player_dismissed][delData.bowler] += 1;

        if (dismissedData[delData.player_dismissed][delData.bowler] >= maxDisCount) {
            maxDisCount = dismissedData[delData.player_dismissed][delData.bowler];
            batsman = delData.player_dismissed;
            bowler = delData.bowler;
        }
        return dismissedData;
    }, {});

    return { Batsman: batsman, Bowler: bowler, "No.TimesDismissed": maxDisCount };
}

module.exports = problem8;
