function problem1(returedArrayOfObjects) {
    let ans = returedArrayOfObjects.data.reduce((acc, objectData) => {
        if (objectData.season in acc) {
            acc[objectData.season] += 1;
        } else {
            acc[objectData.season] = 1;
        }
        return acc;
    }, {});
    return ans;
}

module.exports = problem1;
