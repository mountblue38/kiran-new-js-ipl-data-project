function problem6(returedArrayOfObjects) {
    let resObj = {};
    let MOMcount, MOMplayer;

    let outputObj = returedArrayOfObjects.data.reduce((acc, matchData) => {
        if (!(matchData.season in resObj)) {
            MOMcount = 0;
            MOMplayer = "";
            resObj[matchData.season] = {};
        }

        if (matchData.player_of_match in resObj[matchData.season]) {
            if (resObj[matchData.season][matchData.player_of_match] > MOMcount) {
                MOMcount = resObj[matchData.season][matchData.player_of_match];
                MOMplayer = matchData.player_of_match;
            }
            resObj[matchData.season][matchData.player_of_match] += 1;
        } else {
            resObj[matchData.season][matchData.player_of_match] = 1;
        }

        acc[matchData.season] = MOMplayer;
        return acc;
    }, {});

    return outputObj;
}

module.exports = problem6;
