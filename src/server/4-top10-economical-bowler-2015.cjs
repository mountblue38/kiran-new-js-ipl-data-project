function problem4(deliverysData, matchesData) {
    let filteredData = matchesData.data.filter((element) => {
        return element.season == "2015";
    });

    let ans = filteredData.reduce((ecoObj, matchData) => {
        deliverysData.data.map((delData) => {
            if (!(matchData.season in ecoObj)) {
                ecoObj[matchData.season] = {};
            }
            if (matchData.id == delData.match_id) {
                if (!(delData.bowler in ecoObj[matchData.season])) {
                    ecoObj[matchData.season][delData.bowler] = { bowler: "", runs: 0, balls: 0 };
                }
                ecoObj[matchData.season][delData.bowler].runs += Number(delData.batsman_runs);
                ecoObj[matchData.season][delData.bowler].balls += 1;
                ecoObj[matchData.season][delData.bowler].bowler = delData.bowler;
            }
            return delData;
        });
        return ecoObj;
    }, {});

    let finalArr = Object.keys(ans).reduce((acc, year) => {
        Object.keys(ans[year]).map((player) => {
            let economy = Number(((ans[year][player].runs / ans[year][player].balls) * 6).toFixed(2));
            acc.push({ bowler: ans[year][player].bowler, economy: economy });
            return player;
        });
        return acc;
    }, []);

    function compare(value1, value2) {
        if (value1.economy < value2.economy) {
            return -1;
        }
        if (value1.economy > value2.economy) {
            return 1;
        }
        return 0;
    }

    return finalArr.sort(compare).slice(0, 10);
}

module.exports = problem4;
