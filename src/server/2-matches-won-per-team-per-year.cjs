function problem2(returedArrayOfObjects) {
    let res = returedArrayOfObjects.data.reduce((outputObj, objectData) => {
        if (!(objectData.season in outputObj)) {
            outputObj[objectData.season] = {};
        }
        if (objectData.winner in outputObj[objectData.season]) {
            outputObj[objectData.season][objectData.winner] += 1;
        } else {
            outputObj[objectData.season][objectData.winner] = 1;
        }
        return outputObj;
    }, {});
    return res;
}

module.exports = problem2;
